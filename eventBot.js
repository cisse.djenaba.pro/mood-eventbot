import fetch from "node-fetch";
import eventDao from "./event-dao-mongoose.js";



async function callDataIleDeFrance() {
  //let dataUrl = "https://data.iledefrance.fr/api/records/1.0/search/?dataset=evenements-publics-cibul&q=&rows=10&facet=tags&facet=placename&facet=department&facet=region&facet=city&facet=date_start&facet=date_end&facet=pricing_info&facet=updated_at&facet=city_district&refine.date_end=2022";
  let dataUrl = "https://opendata.paris.fr/api/records/1.0/search/?dataset=que-faire-a-paris-&q=&rows=1000&facet=date_start&facet=date_end&facet=tags&facet=address_name&facet=address_zipcode&facet=address_city&facet=pmr&facet=blind&facet=deaf&facet=transport&facet=price_type&facet=access_type&facet=updated_at&facet=programs";
  let arrayData = [];
  try {
    const response = await fetch(dataUrl);
    //console.log("response.status : ", + response.status);
    if (response.ok) {
      let data = await response.json();
      //console.log("response data : " + JSON.stringify(data));
      if (data) {
        arrayData = collectOpenDataInArray(JSON.stringify(data));
      }
      //console.log(arrayData);
    } else {
      let text = await response.text();
      //console.log("error response text : " + text);
    }
    return arrayData;
  } catch (ex) {
    console.log("ex : " + ex);
  }
}


function collectOpenDataInArray(data) {

  let arrayData = new Array();
  let dataTransform = JSON.parse(data);
  let dataObject = dataTransform["records"];
  let initialiseDate = new Date();
  let todaysDate = initialiseDate.getFullYear() + "-" + ((initialiseDate.getMonth() < 9 ? '0' : '') + (initialiseDate.getMonth() + 1)) + "-" + initialiseDate.getDate();
  for (let index in dataObject) {
    if (dataObject[index].fields.date_end > todaysDate) {
      let eventData = new eventDao.ThisPersistentModel();
      eventData.apiId = dataObject[index].recordid;
      eventData.link = dataObject[index].fields.url;
      eventData.title = dataObject[index].fields.title;
      eventData.endDate = dataObject[index].fields.date_end;
      eventData.startDate = dataObject[index].fields.date_start;
      eventData.image = dataObject[index].fields.cover_url;
      eventData.tags = dataObject[index].fields.tags;
      eventData.description = dataObject[index].fields.description;
      eventData.place = dataObject[index].fields.address_name;
      eventData.address = dataObject[index].fields.address_street;
      eventData.city = dataObject[index].fields.address_city;
      eventData.zip = dataObject[index].fields.address_zipcode;
      eventData.contactUrl = dataObject[index].fields.contact_url;
      eventData.contactMail = dataObject[index].fields.contact_mail;
      eventData.contactPhone = dataObject[index].fields.contact_phone;
      eventData.priceType = dataObject[index].fields.price_type;
      eventData.price = dataObject[index].fields.price_detail;
      eventData.reservationUrl = dataObject[index].fields.access_link;
      eventData.texteReservationUrl = dataObject[index].fields.access_link_text;
      arrayData.push(eventData);
      //console.log(" total de : "  + index)
    }
  }
  return arrayData;
}

async function storeEventInMongoDataBase(event) {
  try {
    let savedEvent = await eventDao.save(event);
    return savedEvent;
  } catch (ex) {
    console.log("ex2 : " + ex);
  }
}


async function retreiveInfos() {
  let arrayData = await callDataIleDeFrance();
  for (let [i, v] of arrayData.entries()) {
    arrayData[i] = await storeEventInMongoDataBase(v);
  }
  //console.log(JSON.stringify(arrayData));
}

retreiveInfos();


/*

var intervaleDeTemps = 15000; //5000ms = 5s
//pour 1h , intervaleDeTemps = 1000 * 60 * 60
var compteur = 0;

//coder une fonction qui sera appelée toutes les 5s (5000ms) et qui va incrementer et afficher le
//compteur. Lorsque le compteur aura atteint la valeur 10 on arrete le traitement périodique
// via un appel à clearInterval(traitementPeriodique)
function codeDeclenchePeriodiquement() {
  compteur++;
  console.log("compteur=" + compteur);
  if (compteur == 10) {
    {
      clearInterval(traitementPeriodique);
      process.exit();
    }
  }
}
var traitementPeriodique = setInterval(
  codeDeclenchePeriodiquement,
  intervaleDeTemps
);

*/